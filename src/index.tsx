import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import ICAOLookup from './components/airportdata-lookup';
import HowToIntro from './components/how-to-use';
import Intro from './components/intro';



ReactDOM.render(
  <React.StrictMode>
    <Intro></Intro>
    <HowToIntro></HowToIntro>

    <Router>
      <Route path="/view">

        <ICAOLookup></ICAOLookup>

      </Route>

      <ul>
        <li>
          <Link to="/view">View</Link>
        </li>
      </ul>

    </Router>


    {/* <button onClick={() => window.open("/view", "_self")}>Click to view airport code</button> */}
    <button onClick={() => window.open("/view", "_self")}>Click to view airport code</button>




  </React.StrictMode>,
  document.getElementById('root')
);


