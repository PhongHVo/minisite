

export default function Intro() {

    return (
        <div>
            <h3>Airport Data with ICAO and IATA Codes (API Documentation)</h3>
            <h3>This application allows you to search for airport ICAO or IATA code.</h3>
        </div>
    )
}